import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    categories: [],
    category: {},
    articles: []
  },
  getters: {
    getCategories: state => state.categories,
    getCategory: state => state.category,
    getArticles: state => state.articles
  },
  actions: {
    async fetchCategories({ commit }) {
      const response = await axios.get('http://localhost:9000/api/categories');
      commit('setCategories', response.data);
    },
    async fetchArticles({ commit }, categoryID) {
      const response = await axios.get(`http://localhost:9000/api/category/${ categoryID }`);
      commit('setArticles', response.data);
    },
    setCategory({ commit }, category) {
      commit('setCategory', category);
    }
  },
  mutations: {
    setCategories: (state, payload) => {
      state.categories = payload;
    },
    setCategory: (state, payload) => {
      state.category = payload;

    },
    setArticles: (state, payload) => {
      state.articles = payload;
    }
  }
})
