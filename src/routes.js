import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './views/Dashboard.vue'
import Article from './views/Article.vue'

Vue.use(VueRouter)

export const router = new VueRouter({
	routes: [
		{ path: '/', component: Home },
		{ path: '/article/:id', component: Article }
	]
})
