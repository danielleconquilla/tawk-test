import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import { router } from './routes'
import moment from 'moment'
import { store } from './store.js'

Vue.prototype.moment = moment

new Vue({
	el: '#app',
	router,
	store,
	render: h => h(App)
});
